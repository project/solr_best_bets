<?php

/**
 * @file
 * Provides an interface for site administrators to configure best bets for
 * searches. Generates the elevate.xml configuration file that can be deployed
 * to an Apache Solr server.
 */

/**
 * Implements hook_menu().
 */
function solr_best_bets_menu() {
  $items = array();

  $items['admin/config/search/solr_best_bets'] = array(
    'title' => 'Solr Best Bets',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('solr_best_bets_admin_form'),
    'access arguments' => array('administer search'),
    'description' => 'Configure best bets and configuration file generation behavior.',
    'file' => 'solr_best_bets.admin.inc',
  );

  $items['admin/config/search/solr_best_bets/configure'] = array(
    'title' => 'Best Bets',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/config/search/solr_best_bets/advanced'] = array(
    'title' => 'Advanced',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('solr_best_bets_advanced_form'),
    'access arguments' => array('administer search'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'solr_best_bets.admin.inc',
    'weight' => 10,
  );

  $items['admin/config/search/solr_best_bets/keygen'] = array(
    'title' => 'Regenerate keys',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('solr_best_bets_keygen_confirm'),
    'access arguments' => array('administer search'),
    'file' => 'solr_best_bets.admin.inc',
  );

  $items['node/%node/solr_best_bets'] = array(
    'title' => 'Best bet queries',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('solr_best_bets_node_overview', 1),
    'access arguments' => array('administer search'),
    'weight' => 10,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'file' => 'solr_best_bets.pages.inc',
  );

  $items['node/%node/solr_best_bets/add'] = array(
    'title' => 'Add as best bet for search query',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('solr_best_bets_node_form', 1),
    'access arguments' => array('administer search'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'solr_best_bets.pages.inc',
  );

  $items['solr/%solr_best_bets_environment/elevate.xml'] = array(
    'title' => 'Apache Solr elevate.xml Configuration File',
    'page callback' => 'solr_best_bets_xml_page',
    'page arguments' => array(1),
    'access callback' => array('solr_best_bets_access_xml'),
    'type' => MENU_CALLBACK,
    'file' => 'solr_best_bets.pages.inc',
  );

  return $items;
}

/**
 * Access callback for the elevate.xml file.
 *
 * @return boolean
 *   Flags whether access is granted to the resource.
 */
function solr_best_bets_access_xml() {
  // Check if the callback is enabled.
  // @todo Move this to hook_menu() and clear menu cache on setting save?
  if (!variable_get('solr_best_bets:access_xml', 0)) {
    return FALSE;
  }

  // Check whether a valid key is required to access the elevate.xml file. If
  // the file is key restricted, check to make sure the key is valid.
  if (variable_get('solr_best_bets:key_restrict')) {
    $valid_key = variable_get('solr_best_bets:key', 'drupal');
    if (!isset($_GET['key']) || $_GET['key'] != $valid_key) {
      watchdog('solr_best_bets', 'Access deined to elevate.xml: invalid key passed.', array(), WATCHDOG_NOTICE);
      return FALSE;
    }
  }

  // Access granted if we get here.
  return TRUE;
}

/**
 * Implements hook_admin_paths().
 */
function solr_best_bets_admin_paths() {
  if (variable_get('node_admin_theme')) {
    return array(
      'node/*/solr_best_bets' => TRUE,
      'node/*/solr_best_bets/add' => TRUE,
    );
  }
}

/**
 * Gets all environment definitions.
 *
 * @return array
 *   An array if environment definitions.
 */
function solr_best_bets_get_environments() {
  $environments = array();
  foreach (module_invoke_all('solr_best_bets_environment_info') as $name => $info) {
    $environments[$name] = $info + array(
      'name' => $name,
      'label' => $name,
      'id options' => array(),
      'unique field' => 'id',
    );
  }
  return $environments;
}

/**
 * Loads a single environment definition.
 *
 * @param string $environment_name
 *   The machine name of the environment.
 *
 * @return array|FALSE
 *   The environment definition, FALSE if the definition isn't valid.
 */
function solr_best_bets_environment_load($environment_name) {
  $environments = solr_best_bets_get_environments();
  return isset($environments[$environment_name]) ? $environments[$environment_name] : FALSE;
}

/**
 * Returns an array of environments keyed by machine name to label.
 *
 * @return array
 *   The environment options.
 */
function solr_best_bets_get_environment_options() {
  $options = array();
  foreach (solr_best_bets_get_environments() as $name => $environment) {
    $options[$name] = $environment['label'];
  }
  return $options;
}

/**
 * Implements hook_solr_best_bets_environment_info().
 *
 * Implemented on behalf of Apache Solr Search Integration.
 */
function apachesolr_solr_best_bets_environment_info() {
  $info = array();
  foreach (apachesolr_load_all_environments() as $env_id => $environment) {
    $info['apachesolr:' . $env_id] = array(
      'label' => 'Apache Solr: ' . $environment['name'],
      'id callbacks' => array('apachesolr_entity_document_id'),
    );
  }
  return $info;
}

/**
 * Implements hook_apachesolr_environment_delete().
 */
function solr_best_bets_apachesolr_environment_delete($environment) {
  $environment_name = 'apachesolr:' . $environment['env_id'];
  db_delete('solr_best_bets')->condition('environment', $environment_name)->execute();
}

/**
 * Transforms entity IDs to the Apache Solr Search Integration index value.
 *
 * @param string $content_id
 *   The ID being transformed.
 *
 * @return string|FALSE
 *   A string containing the transformed ID, FALSE if no transformations were
 *   performed.
 */
function apachesolr_entity_document_id($content_id) {
  $parts = explode(':', $content_id, 3);
  $transform_id = ('entity' == $parts[0] && isset($parts[2]));
  return ($transform_id) ? apachesolr_document_id($parts[2], $parts[1]) : FALSE;
}

/**
 * Implements hook_solr_best_bets_environment_info().
 *
 * Implemented on behalf of Search API. In this instance, environments are
 * synonymous with indexes as Search API's uniqie identifier is constructed
 * using the machine name of the index and not the server connection.
 */
function search_api_solr_best_bets_environment_info() {
  $info = array();
  $servers = search_api_server_load_multiple(FALSE);
  foreach (search_api_index_load_multiple(FALSE) as $index) {
    $server = $servers[$index->server];
    $service = search_api_get_service_info($server->class);
    if ('SearchApiSolrService' == $service['class'] || is_subclass_of($service['class'], 'SearchApiSolrService')) {
      $info['search_api:' . $index->machine_name] = array(
        'label' => 'Search API: ' . $index->name,
        'id callbacks' => array('search_api_entity_document_id'),
        'id options' => array('index_name' => $index->machine_name),
      );
    }
  }
  return $info;
}

/**
 * Implements hook_search_api_server_delete().
 */
function solr_best_bets_search_api_server_delete(SearchApiServer $server) {
  $environment_name = 'search_api:' . $server->machine_name;
  db_delete('solr_best_bets')->condition('environment', $environment_name)->execute();
}

/**
 * Transforms entity IDs to the Search API index value.
 *
 * @param string $content_id
 *   The ID being transformed.
 *
 * @return string|FALSE
 *   A string containing the transformed ID, FALSE if no transformations were
 *   performed.
 */
function search_api_entity_document_id($content_id, $options) {
  $parts = explode(':', $content_id, 3);
  $transform_id = ('entity' == $parts[0] && isset($parts[2]));
  return ($transform_id) ? $options['index_name'] . '-' . $parts[2] : FALSE;
}

/**
 * Transforms content IDs to the indexed value.
 *
 * Iterates over the environment's "id callbacks", stops when we have a match.
 *
 * @param string $content_id
 *   The content ID to be conterted into the index value.
 * @param array $environment
 *   The environment definition.
 *
 * @return string|FALSE
 *   A string containing the transformed ID, FALSE if no transformations were
 *   performed.
 */
function solr_best_bets_transform_id($content_id, array $environment) {
  foreach ($environment['id callbacks'] as $callback) {
    $transformed_id = $callback($content_id, $environment['id options']);
    if (FALSE !== $transformed_id && NULL !== $transformed_id) {
      return $transformed_id;
    }
  }
  return FALSE;
}

/**
 * Removes a best bet by it's entity type and ID.
 *
 * @param string $entity_type
 *   The machine name of the entity.
 * @param in $entity_id
 *   The entity's unique identifier.
 */
function solr_best_bets_delete_by_entity_id($entity_type, $entity_id) {
  $content_id = 'entity:' . $entity_type . ':' . $entity_id;
  db_delete('solr_best_bets')->condition('content_id', $content_id)->execute();
}

/**
 * Implements hook_node_delete().
 */
function solr_best_bets_node_delete($node) {
  solr_best_bets_delete_by_entity_id('node', $node->nid);
}

/**
 * Implements hook_user_delete().
 */
function solr_best_bets_user_delete($account) {
  solr_best_bets_delete_by_entity_id('user', $account->uid);
}

/**
 * Implements hook_comment_delete().
 */
function solr_best_bets_comment_delete($comment) {
  solr_best_bets_delete_by_entity_id('comment', $comment->cid);
}

/**
 * Implements hook_taxonomy_term_delete().
 */
function solr_best_bets_taxonomy_term_delete($term) {
  solr_best_bets_delete_by_entity_id('taxonomy', $term->tid);
}
